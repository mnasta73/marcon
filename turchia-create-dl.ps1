Param([Parameter(Mandatory=$True)][String]$Path)
Import-Csv -Path $Path -Delimiter ";"| ForEach {
$c = New-MailContact -Name $_.Name -Alias $_.sAMAccountName -PrimarySMTPAddress $_.SMTPAddress -ExternalEMailAddress $_.SMTPAddress -OrganizationalUnit "recordati.grp/TRIS/Contacts"
}

