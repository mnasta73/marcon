Param([Parameter(Mandatory=$True)][String]$Path)
Import-Csv -Path $Path -Delimiter ";"| ForEach {
$m = Get-Mailbox $_.saMAccountName
$smtpadd = "smtp:" + $_.SMTPAddress
$ex500add = "X500:" + $_.x500Address
$m | Set-MailBox -EmailAddresses @{Add=$smtpadd}
$m | Set-MailBox -EmailAddresses @{Add=$ex500add}

}

