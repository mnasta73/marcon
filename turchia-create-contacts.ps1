Param([Parameter(Mandatory=$True)][String]$Path)
Import-Csv -Path $Path -Delimiter ";"| ForEach {
$c = New-MailContact -Name $_.Name -Alias $_.sAMAccountName -FirstName $_.FirstName -LastName $_.LastName -PrimarySMTPAddress $_.SMTPAddress -ExternalEMailAddress $_.SMTPAddress -OrganizationalUnit "recordati.grp/TRIS/Contacts"
$emailadd = "X500:" + $_.x500Address
$c | Set-MailContact -EmailAddresses @{Add=$emailadd}
}
